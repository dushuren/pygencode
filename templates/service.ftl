package {{basePackage}}.{{moduleName}}.service;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import com.baomidou.mybatisplus.extension.service.IService;

/**
* {{table.remark}}Service接口
*/
public interface {{table.className}}Service extends IService<{{table.className}}> {
}