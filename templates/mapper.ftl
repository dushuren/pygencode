package {{basePackage}}.{{moduleName}}.repository.mapper;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
public interface {{table.className}}Mapper extends BaseMapper<{{table.className}}> {

}
