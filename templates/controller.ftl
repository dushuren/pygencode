package {{basePackage}}.web.controller.cms;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import {{basePackage}}.{{moduleName}}domain.{{table.className}};
import {{basePackage}}.{{moduleName}}msapi.{{table.className}}FeignService;
import {{basePackage}}.framework.api.R;
import {{basePackage}}.framework.log.annotation.OperLog;
import {{basePackage}}.framework.security.entity.SsoUser;
import {{basePackage}}.framework.security.util.SecurityUtil;
import {{basePackage}}.framework.util.ExcelUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@Api("{{table.remark}}管理")
@RestController
@AllArgsConstructor
@RequestMapping("/{{moduleName}}/{{table.tableCameName | replace(moduleName,"") | uncap_first()}}")
public class {{table.className}}Controller {
	private final {{table.className}}FeignService entityFeignService;
	
	@ApiOperation("{{table.remark}}列表")
    @PreAuthorize("@ps.hasPerm('{{table.tableCameName | replace(moduleName,"") | uncap_first()}}_view')")
    @GetMapping("/list")
    @ResponseBody
    public R<List<{{table.className}}>> list(Page<{{table.className}}> page, {{table.className}} {{table.tableCameName | replace(moduleName,"") | uncap_first()}}) {
		SsoUser ssoUser = SecurityUtil.getUser();
		System.out.println(ssoUser.getUsername());
        Page<{{table.className}}> {{table.tableCameName | replace(moduleName,"") | uncap_first()}}Page = entityFeignService.list({{table.tableCameName | replace(moduleName,"") | uncap_first()}}, page.getCurrent(), page.getSize());
        return R.ok({{table.tableCameName | replace(moduleName,"") | uncap_first()}}Page.getRecords(), {{table.tableCameName | replace(moduleName,"") | uncap_first()}}Page.getTotal());
    }

    @ApiOperation("{{table.remark}}查询")
    @GetMapping("/{id}")
    public R<{{table.className}}> getById(@PathVariable("id") Integer id) {
    	{{table.className}} entity = entityFeignService.getById(id);
        return R.ok(entity);
    }

    @OperLog("{{table.remark}}新增")
    @ApiOperation("{{table.remark}}新增")
    @PreAuthorize("@ps.hasPerm('{{table.tableCameName | replace(moduleName,"") | uncap_first()}}_add')")
    @PostMapping("/save")
    @ResponseBody
    public R<Boolean> save(@Validated @RequestBody {{table.className}} {{table.tableCameName | replace(moduleName,"") | uncap_first()}}) {
    	entityFeignService.save({{table.tableCameName | replace(moduleName,"") | uncap_first()}});
        return R.ok();
    }

    @OperLog("{{table.remark}}修改")
    @ApiOperation("{{table.remark}}修改")
    @PreAuthorize("@ps.hasPerm('{{table.tableCameName | replace(moduleName,"") | uncap_first()}}_edit')")
    @PutMapping("/update")
    @ResponseBody
    public R<Boolean> update(@Validated @RequestBody {{table.className}} {{table.tableCameName | replace(moduleName,"") | uncap_first()}}) {
    	entityFeignService.update({{table.tableCameName | replace(moduleName,"") | uncap_first()}});
        return R.ok();
    }

    @OperLog("{{table.remark}}删除")
    @ApiOperation("{{table.remark}}删除")
    @PreAuthorize("@ps.hasPerm('{{table.tableCameName | replace(moduleName,"") | uncap_first()}}_del')")
    @DeleteMapping("/remove/{id}")
    public R<Boolean> remove(@PathVariable("id") Integer[] id) {
        return R.ok(entityFeignService.remove(id));
    }

    @PreAuthorize("@ps.hasPerm('{{table.tableCameName | replace(moduleName,"") | uncap_first()}}_export')")
    @PostMapping("/export")
    @ResponseBody
    public R<String> export({{table.className}} {{table.tableCameName | replace(moduleName,"") | uncap_first()}}) {
    	List<{{table.className}}> list = entityFeignService.list({{table.tableCameName | replace(moduleName,"") | uncap_first()}});
        ExcelUtil<{{table.className}}> util = new ExcelUtil<{{table.className}}>({{table.className}}.class);
        return util.exportExcel(list, "{{table.remark}}数据");
    }
}