package {{basePackage}}.{{moduleName}}.msapi.hystrix;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import {{basePackage}}.{{moduleName}}.msapi.{{table.className}}FeignService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class {{table.className}}FeignServiceHystrix implements {{table.className}}FeignService {

    @Override
    public {{table.className}} getById(Integer id) {
    log.error("服务未启动");
    return new Article();
    }

    @Override
    public boolean save({{table.className}} {{table.tableCameName}}) {
    log.error("服务未启动");
    return false;
    }

    @Override
    public boolean update({{table.className}} {{table.tableCameName}}) {
    log.error("服务未启动");
    return false;
    }

    @Override
    public boolean remove(Integer[] id) {
    log.error("服务未启动");
    return false;
    }

    @Override
    public List<Article> list({{table.className}} {{table.tableCameName}}) {
    log.error("服务未启动");
    return new ArrayList<>();
    }

    @Override
    public Page<Article> list({{table.className}} {{table.tableCameName}}, long current, long size) {
        log.error("服务未启动");
        return new Page<Article>(current, size);
    }
}