package {{basePackage}}.{{moduleName}}.msapi;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import {{basePackage}}.{{moduleName}}.msapi.hystrix.{{table.className}}FeignServiceHystrix;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@FeignClient(name = "mldong-{{moduleName}}", path = "/", fallback = {{table.className}}FeignServiceHystrix.class)
public interface {{table.className}}FeignService {

public static final String PREFIX = "/msapi/{{moduleName}}/{{table.tableCameName | replace(moduleName,"") | uncap_first()}}";

    @GetMapping(PREFIX + "/page")
    public Page<{{table.className}}> list({{table.className}} {{table.tableCameName}},
    @RequestParam(required = false, defaultValue = "1", name = "current") long current,
    @RequestParam(required = false, defaultValue = "10", name = "size") long size);

    @GetMapping(PREFIX + "/list")
    public  List<{{table.className}}> list({{table.className}} {{table.tableCameName}});

    @GetMapping(PREFIX + "/{id}")
    public {{table.className}} getById(@PathVariable("id") Integer id);

    @PostMapping(PREFIX + "/save")
    public boolean save(@Validated @RequestBody {{table.className}} {{table.tableCameName}});

    @PutMapping(PREFIX + "/update")
    public boolean update(@Validated @RequestBody {{table.className}} {{table.tableCameName}});

    @DeleteMapping(PREFIX + "/remove/{id}")
    public boolean remove(@PathVariable("id") Integer[] id);
}