package {{basePackage}}.{{moduleName}}.domain;

import {{basePackage}}.framework.annotation.Excel;
import {{basePackage}}.framework.data.mybatis.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
* {{table.remark}}对象 {{table.className}}
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("{{table.tableName}}")
public class {{table.className}} {
        private static final long serialVersionUID = 1L;
    {% for column in table.columns %}{%if column.primaryKey %}
        /**
        * {{column.remark}}
        */
        @Excel(name = "{{column.remark}}")
        @TableId
        private  {{column.javaType}} {{column.propertyName}};{% else %}
        /**
        * {{column.remark}}
        */
        @Excel(name = "{{column.remark}}")
        private  {{column.javaType}} {{column.propertyName}};{% endif %}
    {% endfor %}
}
